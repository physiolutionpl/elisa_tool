# ELISA_Tool
ELISA Tool a collection of data analysis tools in enzyme-linked immunosorbent assay

Status: going

## Introduction

The solution is intended to create and test such a model of processing measurement
results, which minimizes the probability of user mistake and the risk of loosing information
about data processing up to the final stage of reporting according to SOP.
ipate in its development. The ELISA Tool (ET) presented at work has been implemented
as an extension of another opensource tool, DataExplore (DE) [8] data analysis program.
The modular structure of the solution will allow its easy implementation in a web services
form under Laboratory Information Management System (LIMS) or Electronic LaboratoryNotebook (ELN)

## Installation

The assumption of the project is the ease of using the plug-in in two ways:
1) development way - requires the python environment to be installed, it is a bit complicated way, but it allows to fully control the operation of the software and its easy modification;
2) end user way - requires prior installation of DataExplore according for the selected system, it should be an easy and pleasant way, but it turned out to be unreliable.

#### Description of the development way

1. Install python (version >= 3.5).
2. Fetch DataExplore from github repository (as zip package or by git tools).
3. Install the dependencies from requirements.txt for DataExplore (use: '''$ pipreqs elisa_tool_plug.py''').
4. Check that executing the "python main.py" command in the /pandastabe directory causes the DataExplore to start correctly.
5. Copy the plug-in file and its elisa_tool_repo directory from gitlab repository to the path /plugins in the DataExplore directory.
6. Install dependencies with requirements.txt ELISA-Tool plug-in.
7. Run "python main.py" in the /pandastabe directory to start DataExplore  with the ELISA-Tool plug-in.
8. This solution works independently from a operating system, we use it successfully under win7 / win10 / linux / MacOS.

#### Description of the end user way

The method worked well at the initial stage of plugin development, until the ELISA-Tool plug-in used the same python modules as DataExplore. The published final version (1.0) requires an inclusion of additional modules, which are not used by DataExplore, such as modules for rendering PDF files and data analysis. Due to signals about problems, we have started working on the preparation of a common DataExplore-ELISA-Tool installer for Windows10, including a set of DataExplore and ELISA-Tool dependencies based on the solution provided in pandastable repository. Now we working on it, and we will report here all advances. Until we finish, we recommend "development way" as the best choice.


* See the [wiki page DataExplore](https://github.com/dmnfarrell/pandastable/wiki/Installation) for more details on installing.
* epydoc documentation: pdf and html.
